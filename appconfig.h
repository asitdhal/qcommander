/*=========================================================================

   Program: QCommander

   Copyright (c) 2018 Asit Dhal
   All rights reserved.

   QCommander is a free software; you can redistribute it and/or modify it.


   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
   ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
   LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
   A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR
   CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
   EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
   PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
   PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
   LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
   NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
   SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

========================================================================*/

#ifndef APPCONFIG_H
#define APPCONFIG_H

#include <QList>
#include <QObject>

enum class COLUMN {
  FILE_NAME,
  FILE_TYPE,
  FILE_SIZE,
  LAST_MODIFIED_TIME,
  LAST_ACCESS_TIME,
  CREATION_TIME,
  PERMISSION
};

enum class SplitFsView { ENABLED, DISABLED };

enum class TimestampFormat { ABSOLUTE, RELATIVE };

enum class FileSizeFormat { BYTES, KB, MB, GB, HUMAN_READABLE };

enum class GroupBy {
  FILE_TYPE,
  PERMISSIONS,
};

QString columnToString(COLUMN col);

class AppConfig {
 public:
  static AppConfig& getInstance();
  QList<COLUMN> activeColumns() const;
  void setActiveColumns(const QList<COLUMN>& activeColumns);
  QList<COLUMN> allColumns() const;

  TimestampFormat getTimestampFormat() const;
  void setTimestampFormat(const TimestampFormat& timestampFormat);

  FileSizeFormat getFileSizeFormat() const;
  void setFileSizeFormat(const FileSizeFormat& fileSizeFormat);

  GroupBy getGroupBy() const;
  void setGroupBy(const GroupBy& groupBy);

  bool getGroupByEnabled() const;
  void setGroupByEnabled(bool groupByEnabled);

  SplitFsView getSplitFsViewStatus() const;
  void setSplitFsViewStatus(const SplitFsView& splitFsViewStatus);

 signals:

 public slots:

 private:
  AppConfig();
  ~AppConfig() = default;
  Q_DISABLE_COPY(AppConfig);

  QList<COLUMN> m_activeColumns;
  QList<COLUMN> m_allColumns;
  TimestampFormat m_timestampFormat;
  FileSizeFormat m_fileSizeFormat;
  GroupBy m_groupBy;
  bool m_groupByEnabled;
  SplitFsView m_splitFsViewStatus;
};

#endif  // APPCONFIG_H
