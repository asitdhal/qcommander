/*=========================================================================

   Program: QCommander

   Copyright (c) 2018 Asit Dhal
   All rights reserved.

   QCommander is a free software; you can redistribute it and/or modify it.


   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
   ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
   LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
   A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR
   CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
   EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
   PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
   PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
   LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
   NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
   SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

========================================================================*/

#include "preferencedialog.h"
#include <QDebug>
#include <QTableWidgetItem>
#include "appconfig.h"
#include "ui_preferencedialog.h"

PreferenceDialog::PreferenceDialog(QWidget* parent)
    : QDialog(parent), ui(new Ui::PreferenceDialog) {
  ui->setupUi(this);
  ui->buttonBox->addButton("Apply", QDialogButtonBox::ApplyRole);
  connect(ui->buttonBox, SIGNAL(accepted()), this, SLOT(accept()));
  connect(ui->buttonBox, SIGNAL(rejected()), this, SLOT(reject()));
  connect(ui->buttonBox, SIGNAL(accepted()), this, SLOT(apply()));
  connect(ui->splitFsCheckBox, SIGNAL(stateChanged(int)), this,
          SLOT(splitFsView()));
  populateColumns();
  populateTimestampGroupBox();
  populateSizeFormatGropuBox();
  populateGroupByGroupBox();
  populateSplitFsViewCheckBox();
}

void PreferenceDialog::apply() {}

void PreferenceDialog::populateColumns() {
  auto& appConfig = AppConfig::getInstance();
  auto allColumns = appConfig.allColumns();
  auto activeColumns = appConfig.activeColumns();

  ui->tableWidget->setColumnCount(2);
  ui->tableWidget->setRowCount(allColumns.size());
  ui->tableWidget->setShowGrid(true);
  ui->tableWidget->setSelectionMode(QAbstractItemView::SingleSelection);
  ui->tableWidget->setSelectionBehavior(QAbstractItemView::SelectRows);
  ui->tableWidget->horizontalHeader()->setStretchLastSection(true);

  QStringList header;
  header << tr("Status") << tr("Description");
  ui->tableWidget->setHorizontalHeaderLabels(header);

  for (int i = 0; i < allColumns.size(); i++) {
    QTableWidgetItem* columnNameItem =
        new QTableWidgetItem(columnToString(allColumns.at(i)));
    QTableWidgetItem* checkBoxItem = new QTableWidgetItem();
    checkBoxItem->data(Qt::CheckStateRole);
    auto foundAt = activeColumns.indexOf(allColumns.at(i));
    if (foundAt != -1) {
      checkBoxItem->setCheckState(Qt::Checked);
    } else {
      checkBoxItem->setCheckState(Qt::Unchecked);
    }
    ui->tableWidget->setItem(i, 0, checkBoxItem);
    ui->tableWidget->setItem(i, 1, columnNameItem);
  }

  ui->tableWidget->resizeColumnsToContents();
}

void PreferenceDialog::populateTimestampGroupBox() {
  auto& appConfig = AppConfig::getInstance();
  if (appConfig.getTimestampFormat() == TimestampFormat::ABSOLUTE) {
    ui->absoluteRadioButton->setChecked(true);
    ui->relativeRadioButton->setChecked(false);
  } else {
    ui->absoluteRadioButton->setChecked(false);
    ui->relativeRadioButton->setChecked(true);
  }

  connect(ui->absoluteRadioButton, SIGNAL(clicked(bool)), this,
          SLOT(timestampFormatChanged()));
  connect(ui->relativeRadioButton, SIGNAL(clicked(bool)), this,
          SLOT(timestampFormatChanged()));
}

void PreferenceDialog::timestampFormatChanged() {
  auto& appConfig = AppConfig::getInstance();
  if (ui->absoluteRadioButton->isChecked()) {
    appConfig.setTimestampFormat(TimestampFormat::ABSOLUTE);
  } else {
    appConfig.setTimestampFormat(TimestampFormat::RELATIVE);
  }
}

void PreferenceDialog::populateSizeFormatGropuBox() {
  auto& appConfig = AppConfig::getInstance();
  switch (appConfig.getFileSizeFormat()) {
    case FileSizeFormat::BYTES:
      ui->bytesRadioButton->setChecked(true);
      break;
    case FileSizeFormat::KB:
      ui->kbRadioButton->setChecked(true);
      break;
    case FileSizeFormat::MB:
      ui->mbRadioButton->setChecked(true);
      break;
    case FileSizeFormat::GB:
      ui->gbRadioButton->setChecked(true);
      break;
    case FileSizeFormat::HUMAN_READABLE:
      ui->humanRedableRadioButton->setChecked(true);
      break;
  }

  connect(ui->bytesRadioButton, SIGNAL(clicked(bool)), this,
          SLOT(fileSizeFormatChanged()));
  connect(ui->kbRadioButton, SIGNAL(clicked(bool)), this,
          SLOT(fileSizeFormatChanged()));
  connect(ui->mbRadioButton, SIGNAL(clicked(bool)), this,
          SLOT(fileSizeFormatChanged()));
  connect(ui->gbRadioButton, SIGNAL(clicked(bool)), this,
          SLOT(fileSizeFormatChanged()));
  connect(ui->humanRedableRadioButton, SIGNAL(clicked(bool)), this,
          SLOT(fileSizeFormatChanged()));
}

void PreferenceDialog::fileSizeFormatChanged() {
  auto& appConfig = AppConfig::getInstance();
  if (ui->bytesRadioButton->isChecked()) {
    appConfig.setFileSizeFormat(FileSizeFormat::BYTES);
  } else if (ui->kbRadioButton->isChecked()) {
    appConfig.setFileSizeFormat(FileSizeFormat::KB);
  } else if (ui->mbRadioButton->isChecked()) {
    appConfig.setFileSizeFormat(FileSizeFormat::MB);
  } else if (ui->gbRadioButton->isChecked()) {
    appConfig.setFileSizeFormat(FileSizeFormat::GB);
  } else {
    appConfig.setFileSizeFormat(FileSizeFormat::HUMAN_READABLE);
  }
}

void PreferenceDialog::populateGroupByGroupBox() {
  auto& appConfig = AppConfig::getInstance();

  bool isEnabled = appConfig.getGroupByEnabled();
  if (isEnabled) {
    ui->groupByGroupBox->setChecked(true);
  } else {
    ui->groupByGroupBox->setChecked(false);
  }

  switch (appConfig.getGroupBy()) {
    case GroupBy::FILE_TYPE:
      ui->fileTypeRadioButton->setChecked(true);
      break;
    case GroupBy::PERMISSIONS:
      ui->permissionRadioButton->setChecked(true);
      break;
  }

  connect(ui->fileTypeRadioButton, SIGNAL(clicked(bool)), this,
          SLOT(groupByChanged()));
  connect(ui->permissionRadioButton, SIGNAL(clicked(bool)), this,
          SLOT(groupByChanged()));
  connect(ui->groupByGroupBox, SIGNAL(clicked(bool)), this,
          SLOT(activateGroupBy()));
}

void PreferenceDialog::groupByChanged() {
  auto& appConfig = AppConfig::getInstance();
  if (ui->fileTypeRadioButton->isChecked()) {
    appConfig.setGroupBy(GroupBy::FILE_TYPE);
  } else {
    appConfig.setGroupBy(GroupBy::PERMISSIONS);
  }
}

void PreferenceDialog::activateGroupBy() {
  auto& appConfig = AppConfig::getInstance();
  if (ui->groupByGroupBox->isChecked()) {
    appConfig.setGroupByEnabled(true);
  } else {
    appConfig.setGroupByEnabled(false);
  }
}

PreferenceDialog::~PreferenceDialog() {
  delete ui;
}

void PreferenceDialog::splitFsView() {
  auto& appConfig = AppConfig::getInstance();
  if (ui->splitFsCheckBox->isChecked()) {
    appConfig.setSplitFsViewStatus(SplitFsView::ENABLED);
  } else {
    appConfig.setSplitFsViewStatus(SplitFsView::DISABLED);
  }
}

void PreferenceDialog::populateSplitFsViewCheckBox() {
  auto& appConfig = AppConfig::getInstance();
  switch (appConfig.getSplitFsViewStatus()) {
    case SplitFsView::ENABLED:
      ui->splitFsCheckBox->setCheckState(Qt::CheckState::Checked);
      break;
    case SplitFsView::DISABLED:
      ui->splitFsCheckBox->setCheckState(Qt::CheckState::Unchecked);
      break;
  }
}
