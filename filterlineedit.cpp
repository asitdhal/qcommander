/*=========================================================================

   Program: QCommander

   Copyright (c) 2018 Asit Dhal
   All rights reserved.

   QCommander is a free software; you can redistribute it and/or modify it.


   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
   ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
   LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
   A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR
   CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
   EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
   PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
   PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
   LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
   NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
   SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

========================================================================*/

#include "filterlineedit.h"

#include <QKeyEvent>
#include <QTimer>

FilterLineEdit::FilterLineEdit(QWidget* parent,
                               QList<FilterLineEdit*>* filters,
                               int columnNum)
    : QLineEdit(parent), m_filters(filters), m_columnNum(columnNum) {
  setPlaceholderText(tr("Filter"));
  setClearButtonEnabled(true);
  setProperty("columnNum", columnNum);

  m_delaySignalTimer = new QTimer(this);
  m_delaySignalTimer->setInterval(500);
  connect(this, SIGNAL(textChanged(QString)), m_delaySignalTimer,
          SLOT(start()));
  connect(m_delaySignalTimer, SIGNAL(timeout()), this,
          SLOT(delayedSignalTimerTriggered()));
  connect(this, SIGNAL(editingFinished()), this,
          SLOT(delayedSignalTimerTriggered()));
}

void FilterLineEdit::clear() {
  QLineEdit::clear();
  delayedSignalTimerTriggered();
}

void FilterLineEdit::setText(const QString& text) {
  QLineEdit::setText(text);
  delayedSignalTimerTriggered();
}

void FilterLineEdit::delayedSignalTimerTriggered() {
  m_delaySignalTimer->stop();
  if (text() != m_lastValue) {
    emit delayedTextChanged(text());
    m_lastValue = text();
  }
}

void FilterLineEdit::keyReleaseEvent(QKeyEvent* event) {
  if (event->key() == Qt::Key_Tab) {
    if (m_columnNum < m_filters->size() - 1) {
      m_filters->at(m_columnNum + 1)->setFocus();
      event->accept();
    }
  } else if (event->key() == Qt::Key_Backtab) {
    if (m_columnNum > 0) {
      m_filters->at(m_columnNum - 1)->setFocus();
      event->accept();
    }
  }
}
