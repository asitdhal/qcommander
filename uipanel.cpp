/*=========================================================================

   Program: QCommander

   Copyright (c) 2018 Asit Dhal
   All rights reserved.

   QCommander is a free software; you can redistribute it and/or modify it.


   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
   ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
   LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
   A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR
   CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
   EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
   PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
   PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
   LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
   NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
   SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

========================================================================*/

#include "uipanel.h"
#include <QFileDialog>
#include <QHBoxLayout>
#include <QLineEdit>
#include <QPushButton>
#include <QTableView>
#include <QVBoxLayout>

UiPanel::UiPanel(QWidget* parent) : QWidget(parent) {
  buildUi();
}

void UiPanel::buildUi() {
  m_fsPath = new QLineEdit(this);
  m_fsBrowseButton = new QPushButton(tr("Browse"), this);
  m_fsView = new FileSystemView(this);
  m_fsView->setSelectionBehavior(QAbstractItemView::SelectRows);
  m_fsView->setAlternatingRowColors(true);
  m_fsView->setSortingEnabled(true);
  m_fsView->setShowGrid(false);

  QHBoxLayout* pathLayout = new QHBoxLayout;
  pathLayout->addWidget(m_fsPath);
  pathLayout->addWidget(m_fsBrowseButton);

  QVBoxLayout* mainLayout = new QVBoxLayout;
  mainLayout->addLayout(pathLayout);
  mainLayout->addWidget(m_fsView);
  setLayout(mainLayout);

  connect(m_fsBrowseButton, SIGNAL(clicked()), this,
          SLOT(onFsBrowseButtonClicked()));
}

QTableView* UiPanel::getFsView() {
  return m_fsView;
}

QString UiPanel::path() const {
  return m_path;
}

void UiPanel::setModel(QAbstractItemModel* model) {
  m_fsView->setModel(model);
}

void UiPanel::setPath(const QString& path) {
  m_path = path;
  m_fsPath->setText(m_path);
  emit pathChanged(m_path);
}

void UiPanel::onFsBrowseButtonClicked() {
  QString dir = QFileDialog::getExistingDirectory(
      this, tr("Open Directory"), "/home",
      QFileDialog::ShowDirsOnly | QFileDialog::DontResolveSymlinks);
  setPath(dir);
}
