/*=========================================================================

   Program: QCommander

   Copyright (c) 2018 Asit Dhal
   All rights reserved.

   QCommander is a free software; you can redistribute it and/or modify it.


   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
   ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
   LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
   A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR
   CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
   EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
   PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
   PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
   LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
   NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
   SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

========================================================================*/

#include "filtertableheader.h"
#include "filterlineedit.h"

#include <QApplication>
#include <QScrollBar>
#include <QTableView>

FilterTableHeader::FilterTableHeader(QTableView* parent)
    : QHeaderView(Qt::Horizontal, parent) {
  setSectionsClickable(true);
  setSortIndicatorShown(true);

  connect(this, SIGNAL(sectionResized(int, int, int)), this,
          SLOT(adjustPositions()));
  connect(parent->horizontalScrollBar(), SIGNAL(valueChanged(int)), this,
          SLOT(adjustPositions()));
  connect(parent->verticalScrollBar(), SIGNAL(valueChanged(int)), this,
          SLOT(adjustPositions()));

  setContextMenuPolicy(Qt::CustomContextMenu);
}

void FilterTableHeader::generateFilters(int number, bool showFirst) {
  // Delete all the current filter widgets
  qDeleteAll(m_filterWidgets);
  m_filterWidgets.clear();

  // And generate a bunch of new ones
  for (int i = 0; i <= number; ++i) {
    FilterLineEdit* l = new FilterLineEdit(this, &m_filterWidgets, i);
    if (!showFirst && i == 0)  // This hides the first input widget which
                               // belongs to the hidden rowid column
      l->setVisible(false);
    else
      l->setVisible(true);
    connect(l, SIGNAL(delayedTextChanged(QString)), this,
            SLOT(inputChanged(QString)));
    m_filterWidgets.push_back(l);
  }

  // Position them correctly
  adjustPositions();
}

QSize FilterTableHeader::sizeHint() const {
  // For the size hint just take the value of the standard implementation and
  // add the height of a input widget to it if necessary
  QSize s = QHeaderView::sizeHint();
  if (m_filterWidgets.size())
    s.setHeight(s.height() + m_filterWidgets.at(0)->sizeHint().height() +
                5);  // The 5 adds just adds some extra space
  return s;
}

void FilterTableHeader::updateGeometries() {
  // If there are any input widgets add a viewport margin to the header to
  // generate some empty space for them which is not affected by scrolling
  if (m_filterWidgets.size())
    setViewportMargins(0, 0, 0, m_filterWidgets.at(0)->sizeHint().height());
  else
    setViewportMargins(0, 0, 0, 0);

  // Now just call the parent implementation and reposition the input widgets
  QHeaderView::updateGeometries();
  adjustPositions();
}

void FilterTableHeader::adjustPositions() {
  // Loop through all widgets
  for (int i = 0; i < m_filterWidgets.size(); ++i) {
    // Get the current widget, move it and resize it
    QWidget* w = m_filterWidgets.at(i);
    if (QApplication::layoutDirection() == Qt::RightToLeft)
      w->move(width() - (sectionPosition(i) + sectionSize(i) - offset()),
              w->sizeHint().height() + 2);  // The two adds some extra space
                                            // between the header label and the
                                            // input widget
    else
      w->move(sectionPosition(i) - offset(),
              w->sizeHint().height() + 2);  // The two adds some extra space
                                            // between the header label and the
                                            // input widget
    w->resize(sectionSize(i), w->sizeHint().height());
  }
}

void FilterTableHeader::inputChanged(const QString& new_value) {
  // Just get the column number and the new value and send them to anybody
  // interested in filter changes
  emit filterChanged(sender()->property("column").toInt(), new_value);
}

void FilterTableHeader::clearFilters() {
  for (FilterLineEdit* filterLineEdit : m_filterWidgets)
    filterLineEdit->clear();
}

void FilterTableHeader::setFilter(int column, const QString& value) {
  if (column < m_filterWidgets.size())
    m_filterWidgets.at(column)->setText(value);
}
