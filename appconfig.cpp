/*=========================================================================

   Program: QCommander

   Copyright (c) 2018 Asit Dhal
   All rights reserved.

   QCommander is a free software; you can redistribute it and/or modify it.


   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
   ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
   LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
   A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR
   CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
   EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
   PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
   PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
   LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
   NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
   SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

========================================================================*/
#include "appconfig.h"

QString columnToString(COLUMN col) {
  switch (col) {
    case COLUMN::FILE_NAME:
      return QObject::tr("Name");
    case COLUMN::FILE_TYPE:
      return QObject::tr("Type");
    case COLUMN::FILE_SIZE:
      return QObject::tr("Size");
    case COLUMN::LAST_MODIFIED_TIME:
      return QObject::tr("Last Modified Time");
    case COLUMN::LAST_ACCESS_TIME:
      return QObject::tr("Last Access Time");
    case COLUMN::CREATION_TIME:
      return QObject::tr("Creation Time");
    case COLUMN::PERMISSION:
      return QObject::tr("File Permission");
  }
}

AppConfig& AppConfig::getInstance() {
  static AppConfig appConfig;
  return appConfig;
}

AppConfig::AppConfig() {
  m_allColumns.append(COLUMN::FILE_NAME);
  m_allColumns.append(COLUMN::FILE_TYPE);
  m_allColumns.append(COLUMN::FILE_SIZE);
  m_allColumns.append(COLUMN::LAST_MODIFIED_TIME);
  m_allColumns.append(COLUMN::LAST_ACCESS_TIME);
  m_allColumns.append(COLUMN::CREATION_TIME);
  m_allColumns.append(COLUMN::PERMISSION);

  m_activeColumns.append(COLUMN::FILE_NAME);
  m_activeColumns.append(COLUMN::FILE_TYPE);
  m_activeColumns.append(COLUMN::FILE_SIZE);
  m_activeColumns.append(COLUMN::LAST_MODIFIED_TIME);
  m_activeColumns.append(COLUMN::LAST_ACCESS_TIME);
  // m_activeColumns.append(COLUMN::CREATION_TIME);
  // m_activeColumns.append(COLUMN::PERMISSION);

  m_fileSizeFormat = FileSizeFormat::HUMAN_READABLE;
  m_timestampFormat = TimestampFormat::ABSOLUTE;
  m_groupByEnabled = false;
  m_groupBy = GroupBy::FILE_TYPE;

  m_splitFsViewStatus = SplitFsView::ENABLED;
}

SplitFsView AppConfig::getSplitFsViewStatus() const {
  return m_splitFsViewStatus;
}

void AppConfig::setSplitFsViewStatus(const SplitFsView& splitFsViewStatus) {
  m_splitFsViewStatus = splitFsViewStatus;
}

bool AppConfig::getGroupByEnabled() const {
  return m_groupByEnabled;
}

void AppConfig::setGroupByEnabled(bool groupByEnabled) {
  m_groupByEnabled = groupByEnabled;
}

GroupBy AppConfig::getGroupBy() const {
  return m_groupBy;
}

void AppConfig::setGroupBy(const GroupBy& groupBy) {
  m_groupBy = groupBy;
}

FileSizeFormat AppConfig::getFileSizeFormat() const {
  return m_fileSizeFormat;
}

void AppConfig::setFileSizeFormat(const FileSizeFormat& fileSizeFormat) {
  m_fileSizeFormat = fileSizeFormat;
}

TimestampFormat AppConfig::getTimestampFormat() const {
  return m_timestampFormat;
}

void AppConfig::setTimestampFormat(const TimestampFormat& timestampFormat) {
  m_timestampFormat = timestampFormat;
}

QList<COLUMN> AppConfig::activeColumns() const {
  return m_activeColumns;
}

void AppConfig::setActiveColumns(const QList<COLUMN>& activeColumns) {
  m_activeColumns = activeColumns;
}

QList<COLUMN> AppConfig::allColumns() const {
  return m_allColumns;
}
