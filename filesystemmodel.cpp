/*=========================================================================

   Program: QCommander

   Copyright (c) 2018 Asit Dhal
   All rights reserved.

   QCommander is a free software; you can redistribute it and/or modify it.


   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
   ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
   LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
   A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR
   CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
   EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
   PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
   PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
   LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
   NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
   SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

========================================================================*/

#include "filesystemmodel.h"
#include <QDateTime>
#include <QDirIterator>
#include <QStandardItemModel>

FileSystemModel::FileSystemModel(QObject* parent)
    : QAbstractTableModel(parent), m_currentDir("") {}

void FileSystemModel::load(QString currentPath) {
  m_currentDir = currentPath;
  QDirIterator itr(m_currentDir);

  beginResetModel();
  m_dirEntries.clear();
  endResetModel();

  while (itr.hasNext()) {
    m_dirEntries.append(QFileInfo(itr.next()));
  }
}

void FileSystemModel::changePath(QString newPath) {
  load(newPath);
  emit QAbstractItemModel::dataChanged(createIndex(0, 0),
                                       createIndex(m_dirEntries.size(), 6));
}

QVariant FileSystemModel::headerData(int section,
                                     Qt::Orientation orientation,
                                     int role) const {
  if (role != Qt::DisplayRole) {
    return QVariant();
  }

  if (orientation == Qt::Horizontal) {
    switch (section) {
      case 0:
        return QString(tr("Filename"));
      case 1:
        return QString(tr("Type"));
      case 2:
        return QString(tr("Size"));
      case 3:
        return QString(tr("Last Modified Time"));
      case 4:
        return QString(tr("Last Read Time"));
      case 5:
        return QString(tr("Creation Time"));
      default:
        return QVariant();
    }
  } else if (orientation == Qt::Vertical) {
    return QString("%1").arg(section + 1);
  } else {
    return QVariant();
  }
}

int FileSystemModel::rowCount(const QModelIndex& parent) const {
  Q_UNUSED(parent);
  return m_dirEntries.size();
}

int FileSystemModel::columnCount(const QModelIndex& parent) const {
  Q_UNUSED(parent);
  return 6;
}

QVariant FileSystemModel::data(const QModelIndex& index, int role) const {
  if (!index.isValid())
    return QVariant();

  if (index.row() >= m_dirEntries.size() || index.row() < 0)
    return QVariant();

  if (role == Qt::DisplayRole) {
    QFileInfo fileInfo = m_dirEntries.at(index.row());
    switch (index.column()) {
      case 0:
        return fileInfo.fileName();
      case 1: {
        if (fileInfo.isDir())
          return QString(tr("Dir"));
        else if (fileInfo.isFile())
          return fileInfo.completeSuffix();
        else
          return QVariant();
      }
      case 2:
        return fileInfo.size();
      case 3:
        return fileInfo.lastModified();
      case 4:
        return fileInfo.lastRead();
      case 5:
      default:
        return QVariant();
    }
  }

  return QVariant();
}
