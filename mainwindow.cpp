/*=========================================================================

   Program: QCommander

   Copyright (c) 2018 Asit Dhal
   All rights reserved.

   QCommander is a free software; you can redistribute it and/or modify it.


   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
   ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
   LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
   A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR
   CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
   EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
   PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
   PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
   LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
   NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
   SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

========================================================================*/

#include "mainwindow.h"
#include <QAbstractItemView>
#include <QDir>
#include <QFileDialog>
#include <QHBoxLayout>
#include <QMenu>
#include <QMenuBar>
#include <QTableView>
#include <QToolBar>
#include "uipanel.h"

MainWindow::MainWindow(QWidget* parent)
    : QMainWindow(parent), m_preferenceDialog(new PreferenceDialog(this)) {
  createActions();
  createWidgets();
  createModels();

  showMaximized();

  m_leftPanel->setPath(QDir().currentPath());
  m_rightPanel->setPath(QDir().currentPath());
  m_leftFileSystemModel->load(QDir().currentPath());
  m_rightFileSystemModel->load(QDir().currentPath());
  m_leftPanel->getFsView()->resizeColumnsToContents();
  m_rightPanel->getFsView()->resizeColumnsToContents();
}

void MainWindow::createActions() {
  QMenu* fileMenu = menuBar()->addMenu(tr("&File"));
  QAction* newAct = new QAction(tr("&New"), this);
  newAct->setShortcuts(QKeySequence::New);
  newAct->setStatusTip(tr("Create a new file"));
  connect(newAct, &QAction::triggered, this, &MainWindow::newFile);
  fileMenu->addAction(newAct);

  QAction* preferenceAct = new QAction(tr("Show Preference"), this);
  preferenceAct->setStatusTip(tr("Show Preference"));
  connect(preferenceAct, &QAction::triggered, this,
          &MainWindow::showPreferenceDialog);
  fileMenu->addAction(preferenceAct);

  QAction* exitAct = fileMenu->addAction(tr("E&xit"), this, &QWidget::close);
  exitAct->setShortcuts(QKeySequence::Quit);
  exitAct->setStatusTip(tr("Exit the application"));
}

void MainWindow::newFile() {}

void MainWindow::createWidgets() {
  m_leftPanel = new UiPanel;
  m_rightPanel = new UiPanel;

  QHBoxLayout* mainLayout = new QHBoxLayout;
  mainLayout->addWidget(m_leftPanel);
  // mainLayout->addWidget(m_rightPanel);
  QWidget* centralWidget = new QWidget;
  centralWidget->setLayout(mainLayout);
  setCentralWidget(centralWidget);
}

void MainWindow::createModels() {
  m_leftFileSystemModel = new FileSystemModel;
  m_rightFileSystemModel = new FileSystemModel;
  m_leftProxyModel = new FileSystemSortFilterProxyModel;
  m_rightProxyModel = new FileSystemSortFilterProxyModel;
  m_leftProxyModel->setSourceModel(m_leftFileSystemModel);
  m_rightProxyModel->setSourceModel(m_rightFileSystemModel);
  m_leftProxyModel->setDynamicSortFilter(true);
  m_rightProxyModel->setDynamicSortFilter(true);
  m_leftPanel->setModel(m_leftFileSystemModel);
  m_rightPanel->setModel(m_rightFileSystemModel);
}

void MainWindow::showPreferenceDialog() {
  m_preferenceDialog->show();
}

MainWindow::~MainWindow() {}
