/*=========================================================================

   Program: QCommander

   Copyright (c) 2018 Asit Dhal
   All rights reserved.

   QCommander is a free software; you can redistribute it and/or modify it.


   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
   ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
   LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
   A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR
   CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
   EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
   PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
   PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
   LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
   NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
   SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

========================================================================*/

#include "filesystemview.h"
#include <QHeaderView>
#include <QMenu>
#include "filtertableheader.h"

FileSystemView::FileSystemView(QWidget* parent) : QTableView(parent) {
  m_header = new FilterTableHeader(this);
  m_header->generateFilters(6, true);
  setHorizontalHeader(m_header);
  // m_header = horizontalHeader();
  m_header->setSectionsMovable(true);
  m_header->setContextMenuPolicy(Qt::CustomContextMenu);
  connect(m_header, SIGNAL(customContextMenuRequested(const QPoint&)), this,
          SLOT(showHeaderMenu(const QPoint&)));
  m_hideAction = new QAction(tr("Hide"), this);
  m_moveLast = new QAction(tr("Move Last"), this);
  m_moveFirst = new QAction(tr("Move First"), this);

  connect(m_hideAction, SIGNAL(triggered(bool)), this, SLOT(slotHideColumn()));
  connect(m_moveLast, SIGNAL(triggered(bool)), this, SLOT(slotMoveLast()));
  connect(m_moveFirst, SIGNAL(triggered(bool)), this, SLOT(slotMoveFirst()));
}

void FileSystemView::showHeaderMenu(QPoint p) {
  QMenu menu;
  QPoint globalPoint = mapToGlobal(p);
  int logicalIndex = m_header->logicalIndexAt(p);
  m_hideAction->setData(logicalIndex);

  if (model()->columnCount() - m_currentHiddenColumn.size() > 1) {
    QString nameCurrent =
        model()->headerData(logicalIndex, Qt::Horizontal).toString();
    m_hideAction->setText(tr("Hide ") + nameCurrent);
    menu.addAction(m_hideAction);
  }

  menu.addAction(m_moveLast);
  m_moveLast->setData(logicalIndex);

  menu.addAction(m_moveFirst);
  m_moveFirst->setData(logicalIndex);

  for (int num : m_currentHiddenColumn) {
    QString name = model()->headerData(num, Qt::Horizontal).toString();
    menu.addAction(tr("Show") + name, this, SLOT(showHiddenColumn()));
    menu.actions().last()->setData(num);
  }

  menu.exec(globalPoint);
}

void FileSystemView::slotHideColumn() {
  int colNum = m_hideAction->data().toInt();
  m_currentHiddenColumn.append(colNum);
  hideColumn(colNum);
}

void FileSystemView::showHiddenColumn() {
  QAction* act = (QAction*)sender();
  int ind = act->data().toInt();
  showColumn(ind);
  m_currentHiddenColumn.removeOne(ind);
}

void FileSystemView::slotMoveLast() {
  int last = m_header->count() - 1;
  int current = m_moveLast->data().toInt();
  int from = m_header->visualIndex(current);

  m_header->moveSection(from, last);
}

void FileSystemView::slotMoveFirst() {
  int current = m_moveFirst->data().toInt();
  int from = m_header->visualIndex(current);
  m_header->moveSection(from, 0);
}
