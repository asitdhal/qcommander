/*=========================================================================

   Program: QCommander

   Copyright (c) 2018 Asit Dhal
   All rights reserved.

   QCommander is a free software; you can redistribute it and/or modify it.


   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
   ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
   LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
   A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR
   CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
   EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
   PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
   PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
   LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
   NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
   SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

========================================================================*/

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "filesystemmodel.h"
#include "filesystemsortfilterproxymodel.h"
#include "preferencedialog.h"

class UiPanel;
// namespace Ui {
// class MainWindow;
//}

class FileSystemModel;
class Preferences;

class MainWindow : public QMainWindow {
  Q_OBJECT

 public:
  explicit MainWindow(QWidget* parent = 0);
  ~MainWindow();

 private slots:
  void showPreferenceDialog();
  void newFile();

 private:
  // Ui::MainWindow* ui;

  void createWidgets();
  void createModels();
  void createActions();

  UiPanel* m_leftPanel;
  UiPanel* m_rightPanel;

  FileSystemModel* m_leftFileSystemModel;
  FileSystemModel* m_rightFileSystemModel;
  FileSystemSortFilterProxyModel* m_leftProxyModel;
  FileSystemSortFilterProxyModel* m_rightProxyModel;

  PreferenceDialog* m_preferenceDialog;
};

#endif  // MAINWINDOW_H
