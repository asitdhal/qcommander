/*=========================================================================

   Program: QCommander

   Copyright (c) 2018 Asit Dhal
   All rights reserved.

   QCommander is a free software; you can redistribute it and/or modify it.


   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
   ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
   LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
   A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR
   CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
   EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
   PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
   PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
   LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
   NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
   SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

========================================================================*/

#ifndef UIPANEL_H
#define UIPANEL_H

#include <QObject>
#include <QWidget>
#include "filesystemview.h"

class QTableView;
class QPushButton;
class QLineEdit;
class QAbstractItemModel;

class UiPanel : public QWidget {
  Q_OBJECT
 public:
  explicit UiPanel(QWidget* parent = nullptr);

  QString path() const;
  void setPath(const QString& path);
  QTableView* getFsView();
  void setModel(QAbstractItemModel* model);

 signals:
  void pathChanged(QString path);

 public slots:
  void onFsBrowseButtonClicked();

 private:
  void buildUi();
  QString m_path;
  FileSystemView* m_fsView;
  QPushButton* m_fsBrowseButton;
  QLineEdit* m_fsPath;
};

#endif  // UIPANEL_H
