/*=========================================================================

   Program: QCommander

   Copyright (c) 2018 Asit Dhal
   All rights reserved.

   QCommander is a free software; you can redistribute it and/or modify it.


   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
   ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
   LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
   A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR
   CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
   EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
   PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
   PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
   LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
   NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
   SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

========================================================================*/

#include "filesystemsortfilterproxymodel.h"
#include <QDateTime>

FileSystemSortFilterProxyModel::FileSystemSortFilterProxyModel(QObject* parent)
    : QSortFilterProxyModel(parent) {}

QString FileSystemSortFilterProxyModel::fileType() const {
  return m_fileType;
}

void FileSystemSortFilterProxyModel::setFileType(const QString& fileType) {
  m_fileType = fileType;
  invalidateFilter();
}

void FileSystemSortFilterProxyModel::clearFilters() {
  m_fileType.clear();
  invalidateFilter();
}

bool FileSystemSortFilterProxyModel::filterAcceptsRow(
    int sourceRow,
    const QModelIndex& sourceParent) const {
  if (!m_fileType.isEmpty()) {
    QModelIndex index = sourceModel()->index(sourceRow, 2, sourceParent);
    if (m_fileType != sourceModel()->data(index).toString())
      return false;
  }
  return true;
}

bool FileSystemSortFilterProxyModel::lessThan(const QModelIndex& left,
                                              const QModelIndex& right) const {
  QVariant leftData = sourceModel()->data(left);
  QVariant rightData = sourceModel()->data(right);

  if (left.column() == 0) {
    return QString::localeAwareCompare(leftData.toString(),
                                       rightData.toString()) < 0;
  } else if (left.column() == 1) {
    return QString::compare(leftData.toString(), rightData.toString(),
                            Qt::CaseInsensitive) < 0;
  } else if (left.column() == 2) {
    return leftData.toUInt() < rightData.toUInt();
  } else if (left.column() == 3 || left.column() == 4 || left.column() == 5) {
    return leftData.toDateTime() < rightData.toDateTime();
  } else {
    return QString::localeAwareCompare(leftData.toString(),
                                       rightData.toString()) < 0;
  }
}
