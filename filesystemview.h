/*=========================================================================

   Program: QCommander

   Copyright (c) 2018 Asit Dhal
   All rights reserved.

   QCommander is a free software; you can redistribute it and/or modify it.


   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
   ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
   LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
   A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR
   CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
   EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
   PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
   PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
   LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
   NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
   SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

========================================================================*/

#ifndef FILESYSTEMVIEW_H
#define FILESYSTEMVIEW_H

#include <QTableView>

class FilterTableHeader;

class QHeaderView;

class FileSystemView : public QTableView {
  Q_OBJECT

 public:
  FileSystemView(QWidget* parent = 0);
  ~FileSystemView() = default;

 private slots:
  void showHeaderMenu(QPoint p);
  void slotHideColumn();
  void showHiddenColumn();
  void slotMoveLast();
  void slotMoveFirst();

 private:
  FilterTableHeader* m_header;
  QAction* m_hideAction;
  QAction* m_moveLast;
  QAction* m_moveFirst;

  QList<int> m_currentHiddenColumn;
};

#endif  // FILESYSTEMVIEW_H
